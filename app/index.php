<?php
namespace LazyMonad;

require __DIR__ . '/../vendor/autoload.php';

use LazyMonad\Libs\Dependencies;
use LazyMonad\Libs\Errors;
use LazyMonad\Libs\Router;

// Error handling
new Errors();
// DI
$di = new Dependencies();
// Routes handler
new Router($di->getInjector());
