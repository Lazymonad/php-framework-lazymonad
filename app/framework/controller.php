<?php
namespace LazyMonad\Framework;

use Http\Request;
use Http\Response;

/**
 * TODO:
 * Rendering engine maybe twig????
 */
abstract class Controller
{
    public $response;
    public $request;

    public function __construct(Request $request, Response $response)
    {
        $this->response = $response;
        $this->request = $request;
    }

    public function view(string $content)
    {
        // should be twig
        echo $content;
    }

}
