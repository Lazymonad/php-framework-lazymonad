<?php

return [
    ['GET', '/', ['LazyMonad\Controllers\Homepage', 'show']],
    ['GET', '/error', ['LazyMonad\Controllers\Homepage', 'error']],
];
