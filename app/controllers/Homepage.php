<?php
namespace LazyMonad\Controllers;

use LazyMonad\Framework\Controller;

class Homepage extends Controller
{
    public function show()
    {
        $content = '<h1>This is homepage</h1>';
        $content .= 'Hello ' . $this->request->getParameter('name', 'stranger');
        $this->view($content);
    }

    public function error()
    {
        $this->view("Klaida");
    }
}
