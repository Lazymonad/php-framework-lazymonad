<?php
namespace LazyMonad\Libs;

class Errors
{

    public function __construct()
    {
        if (\LazyMonad\Config::$env === 'dev') {
            $this->init();
        }
    }

    /**
     * init
     * Register Whoops package to prettify error handling
     *
     * @return void
     */
    private function init(): void
    {
        error_reporting(E_ALL);

        $whoops = new \Whoops\Run;
        $whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
        $whoops->register();
    }
}
