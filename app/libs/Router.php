<?php
namespace LazyMonad\Libs;

use \Auryn\Injector;

class Router
{
    private $injector;
    private $dispatcher;
    private $response;
    private $request;

    /**
     * constructor
     *
     * @param Injector $injector
     */
    public function __construct(Injector $injector)
    {
        $this->injector = $injector;
        $this->request = $injector->make('Http\HttpRequest');
        $this->response = $injector->make('Http\HttpResponse');

        $routeDefinitionCallback = function (\FastRoute\RouteCollector $r) {
            $routes = include __DIR__ . '/../Routes.php';
            foreach ($routes as $route) {
                $r->addRoute($route[0], $route[1], $route[2]);
            }
        };

        $this->dispatcher = \FastRoute\simpleDispatcher($routeDefinitionCallback);

        $this->init();
    }

    /**
     * Init
     *
     * @return void
     */
    private function init(): void
    {
        $routeInfo = $this->dispatcher->dispatch($this->request->getMethod(), $this->request->getPath());
        switch ($routeInfo[0]) {
            case \FastRoute\Dispatcher::NOT_FOUND:
                $response->setContent('404 - Page not found');
                $response->setStatusCode(404);
                break;
            case \FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
                $response->setContent('405 - Method not allowed');
                $response->setStatusCode(405);
                break;
            case \FastRoute\Dispatcher::FOUND:
                $className = $routeInfo[1][0];
                $method = $routeInfo[1][1];
                $vars = $routeInfo[2];

                $class = $this->injector->make($className);
                $class->$method($vars);
                break;
        }
    }
}
