<?php
namespace LazyMonad\Libs;

use \Auryn\Injector;

class Dependencies
{
    private $injector;

    public function __construct()
    {
        $this->injector = new Injector;
        $this->init();
    }

    private function init(): void
    {
        $this->injector->alias('Http\Request', 'Http\HttpRequest');
        $this->injector->share('Http\HttpRequest');
        $this->injector->define('Http\HttpRequest', [
            ':get' => $_GET,
            ':post' => $_POST,
            ':cookies' => $_COOKIE,
            ':files' => $_FILES,
            ':server' => $_SERVER,
        ]);

        $this->injector->alias('Http\Response', 'Http\HttpResponse');
        $this->injector->share('Http\HttpResponse');
    }

    public function getInjector(): Injector
    {
        return $this->injector;
    }
}
